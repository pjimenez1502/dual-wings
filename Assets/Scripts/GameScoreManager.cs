﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameScoreManager : NetworkBehaviour
{
    [SyncVar]
    int score;

    void Update()
    {
        GameObject.Find("HUD").GetComponent<HudManager>().SetScore(score);
    }

    internal void AddScore(int scoreValue)
    {
        score += scoreValue;
    }

    public void ExitGame()
    {
        NetworkManager.singleton.StopHost();
    }

    public void GameOverGame()
    {
        NetworkManager.singleton.StopHost();
        GameObject.Find("NetworkManager").GetComponent<FirebaseUpload>().FirebaseUploadData(score);
    }
}
