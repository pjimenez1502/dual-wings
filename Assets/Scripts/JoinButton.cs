﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

public class JoinButton : MonoBehaviour
{
    private Text buttonText;
    private MatchInfoSnapshot match;

    private void Awake()
    {
        buttonText = GetComponentInChildren<Text>();
        GetComponent<Button>().onClick.AddListener(JoinMatch);
    }

    public void Initialize(MatchInfoSnapshot match, Transform panelTransform)
    {
        this.match = match;

        buttonText.text = match.name;
        transform.SetParent(panelTransform);
        transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        transform.localRotation = Quaternion.Euler(new Vector3(90, 0, 0));
        transform.localPosition = Vector3.zero;
    }

    private void JoinMatch()
    {
        FindObjectOfType<CustomNetworkManager>().JoinMatch(match);
    }

}
