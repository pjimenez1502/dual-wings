﻿
using UnityEngine;
using UnityEngine.Networking;

public class DestroyByContact : NetworkBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    private HudManager hudManager;
    GameScoreManager scoreMan;

    void Start()
    {
        GameObject hud = GameObject.Find("HUD");
        hudManager = hud.GetComponent<HudManager>();

        scoreMan = GameObject.Find("ScoreManager").GetComponent<GameScoreManager>();
    }


    void OnTriggerEnter(Collider other)
    {
        if (NetworkServer.active)
        {
            //switch collision detections
            switch (tag)
            {
                case "Player":
                    if (other.tag == "EnemyBullet")
                    {
                        GetComponent<PlayerController>().damage();
                        NetworkServer.Spawn(Instantiate(playerExplosion, other.gameObject.transform.position, other.gameObject.transform.rotation));
                        Destroy(other.gameObject);
                    }

                    break;

                case "Tracker":
                    if (other.tag == "Player")
                    {
                        other.GetComponent<PlayerController>().damage();
                        NetworkServer.Spawn(Instantiate(playerExplosion, other.gameObject.transform.position, other.gameObject.transform.rotation));
                        Destroy(gameObject);
                    }
                    else if (other.tag == "PlayerBullet")
                    {
                        NetworkServer.Spawn(Instantiate(explosion, transform.position, transform.rotation));
                        Destroy(other.gameObject);
                        Destroy(gameObject);
                        scoreMan.AddScore(scoreValue);
                    }

                    break;

                case "Marauder":
                    if (other.tag == "Player")
                    {
                        NetworkServer.Spawn(Instantiate(playerExplosion, other.gameObject.transform.position, other.gameObject.transform.rotation));
                        other.GetComponent<PlayerController>().damage();
                        Destroy(gameObject);
                    }
                    else if (other.tag == "PlayerBullet")
                    {
                        NetworkServer.Spawn(Instantiate(explosion, transform.position, transform.rotation));
                        GetComponent<MarauderController>().damage();
                        Destroy(other.gameObject);
                        scoreMan.AddScore(scoreValue);
                    }
                    break;
            }

        }
    }

}
