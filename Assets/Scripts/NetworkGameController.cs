﻿

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

public class NetworkGameController : NetworkBehaviour
{
    public GameObject trackerPrefab;
    public GameObject marauderPrefab;
    public Vector3 spawnValues;

    public string name;
    public int marauderCount;
    public int trackerCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    int score;

    private bool started;
    private int roundNum;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("HUD").GetComponent<HudManager>().GetReady())
        {
            if (!started)
            {
                started = true;
                StartCoroutine(SpawnWaves());
            }
        }

    }
   
    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < marauderCount; i++)
            {
                RpcSpawnMarauder();
                yield return new WaitForSeconds(spawnWait);
            }
            
            for (int i = 0; i < trackerCount; i++)
            {
                RpcSpawnTracker();
                yield return new WaitForSeconds(spawnWait);
            }

            yield return new WaitForSeconds(waveWait);

            roundNum++;
        }
    }

    //Spawn Enemies

    void RpcSpawnTracker()
    {
        Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
        Quaternion spawnRotation = new Quaternion(0, 180, 0, 0);

        GameObject enemy = Instantiate(trackerPrefab, spawnPosition, spawnRotation);
        NetworkServer.Spawn(enemy);

    }


    void RpcSpawnMarauder()
    {

        Vector3 spawnPosition;

        if (Random.Range(0,2) == 1)
        {
            spawnPosition = new Vector3(Random.Range(-9, -7), spawnValues.y, Random.Range(12, 17));
        }
        else
        {
            spawnPosition = new Vector3(Random.Range(7, 9), spawnValues.y, Random.Range(12, 17));
        }
        
        Quaternion spawnRotation = new Quaternion(0, 180, 0, 0);

        GameObject enemy = Instantiate(marauderPrefab, spawnPosition, spawnRotation);
        NetworkServer.Spawn(enemy);

    }


}
