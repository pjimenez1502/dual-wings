﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorePanel : MonoBehaviour
{
    [SerializeField]
    public ScoreListObject scoreObjectPrefab;

    public void UpdatingList(List<int> scorelist)
    {
        ClearList();
        CreateList(scorelist);
    }

    void ClearList()
    {
        var scoreObjs = GetComponentsInChildren<ScoreListObject>();
        foreach(var scoreObj in scoreObjs)
        {
            Destroy(scoreObj.gameObject);
        }
    }

    void CreateList(List<int> scorelist)
    {
        int index = 0;

        foreach (var score in scorelist)
        {
            var listEl = Instantiate(scoreObjectPrefab);
            listEl.Initialize(score, transform, index);
            index++;
        }

    }

}
