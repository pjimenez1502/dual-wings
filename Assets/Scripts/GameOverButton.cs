﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverButton : MonoBehaviour
{
    void Awake()
    {
        GetComponent<Button>().onClick.AddListener(GameObject.Find("ScoreManager").GetComponent<GameScoreManager>().GameOverGame);
    }
}
