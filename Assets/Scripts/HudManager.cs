﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class HudManager : MonoBehaviour
{
    private bool gameOver;
    private bool ready;

    private int score;

    private PlayerController player01;
    private PlayerController player02;

    private GameObject hbRed;
    private GameObject hbBlue;
    private GameObject scoreText;

    private GameObject healthBar;

    private GameObject waitMenu;
    private GameObject gameOverMenu;
    

    void Start()
    {
        score = 0;
        hbRed = GameObject.Find("RedContainer");
        hbBlue = GameObject.Find("BlueContainer");

        healthBar = GameObject.Find("HealthBar");
        scoreText = GameObject.Find("ScoreText");

        waitMenu = GameObject.Find("WaitingCanvas");
        gameOverMenu = GameObject.Find("GameOverCanvas");

        healthBar.SetActive(false);
        scoreText.SetActive(false);
        gameOverMenu.SetActive(false);
        
    }

    private void Update()
    {
        if (GameObject.FindGameObjectsWithTag("Player").Length == 2)
        {
            if (!ready)
            {
                Debug.Log(waitMenu);
                waitMenu.SetActive(false);
                ready = true;
                GetPlayers();
            }
        }

        if (healthBar.activeInHierarchy)
        {
            hbRed.transform.localScale = new Vector3(1, (player01.GetHealth() / 5), 0);
            hbBlue.transform.localScale = new Vector3(1, (player02.GetHealth() / 5), 0);

            if(player01 == null)
                hbRed.transform.localScale = new Vector3(1, 0, 0);
            if (player02 == null)
                hbBlue.transform.localScale = new Vector3(1, 0, 0);

            scoreText.GetComponent<TextMeshProUGUI>().text = score.ToString();
        }

        if (ready)
        {
            if (player01 == null && player02 == null)
            {
                if (!gameOver)
                    GameOver();
            }
        }
    }

    private void GameOver()
    {
        Debug.Log("GameOver");
        gameOver = true;
        gameOverMenu.SetActiveRecursively(true);
        GameObject.Find("ScoreNum").GetComponent<TextMeshProUGUI>().text = score.ToString();

        healthBar.SetActive(false);
        scoreText.SetActive(false);
    }

    public void GetPlayers()
    {
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (player.GetComponent<PlayerController>().isLocalPlayer == true)
            {
                player01 = player.GetComponent<PlayerController>();
            }
            else
            {
                player02 = player.GetComponent<PlayerController>();
            }
        }

        healthBar.SetActive(true);
        scoreText.SetActive(true);
    }



    public void SetScore(int newScoreValue)
    {
        score = newScoreValue;
    }

    internal bool GetReady()
    {
        return ready;
    }


    

}

