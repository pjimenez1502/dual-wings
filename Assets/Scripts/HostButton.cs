﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HostButton : MonoBehaviour
{
    void Awake()
    {
        GetComponent<Button>().onClick.AddListener(StartMatch);
    }

    void StartMatch()
    {
        GameObject.Find("MatchListPlane").SetActive(false);
        GameObject.Find("NetworkManager").GetComponent<CustomNetworkManager>().StartHosting();
    }

}
