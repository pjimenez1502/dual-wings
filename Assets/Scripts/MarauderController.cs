﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class MarauderController : NetworkBehaviour
{
    public int health;

    //Movement vars
    public float moveSpeed;
    public float rotationSpeed;
    public Vector3 cageCoord;

    bool inside = false;
    bool arrived = false;
    float laps;

    //Firing Vars
    public float fireRate;
    private float nextFire;
    private float enemyTime = 0;

    private GameObject newProjectile;
    public GameObject shot;
    public Transform shotSpawn;

        

    void Start()
    {
        getNewCoords();
    }
    // Update is called once per frame
    void Update()
    {

        //arrived is true when ship gets to destination
        //inside is false until ship arrives inside for the first time

        if (arrived)
        {
            if (inside == false)
            {
                inside = true;
            }
            //if it's been on screen for a while, flee
            if (laps > 4)
            {
                flee();
            }
            else
            //get new coord to move to
            {
                getNewCoords();
                laps++;
            }
        }
        
        float step = moveSpeed * Time.deltaTime; // calculate distance to move
        transform.position = Vector3.MoveTowards(transform.position, cageCoord, step);
        
        //if it has arrived to destination
        if (cageCoord == transform.position)
        {
            arrived = true;
        }

        //if has entered scene
        if (inside)
        {
            enemyTime = enemyTime + Time.deltaTime;
            if (enemyTime > nextFire)
            {
                Fire();

                nextFire = Random.Range(2, 4);
                enemyTime = 0.0F;
            }
        }
    }

    void getNewCoords()
    {
        arrived = false;
        cageCoord =  new Vector3(Random.Range(-5, 5), 0, Random.Range(11, 16));
    }

    public void flee()
    {
        arrived = false;
        if (Random.Range(0, 2) == 1)
        {
            cageCoord = new Vector3(Random.Range(-9, -7), 0, Random.Range(12, 17));
        }
        else
        {
            cageCoord = new Vector3(Random.Range(7, 9), 0, Random.Range(12, 17));
        }
    }


    // Creates the projectiles and fires them
    void Fire()
    {
        newProjectile = Instantiate(shot, shotSpawn.position, shotSpawn.rotation) as GameObject;
        NetworkServer.Spawn(newProjectile);
    }


    public void damage()
    {
        health--;

        if (health <= 0)
        {
            Death(transform.gameObject);
        }
    }

    void Death(GameObject marauder)
    {
        NetworkServer.Destroy(marauder);
    }
}
