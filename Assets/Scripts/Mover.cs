﻿
using UnityEngine;
using UnityEngine.Networking;

public class Mover : MonoBehaviour
{
    public float speed;

    private void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
    }

    public void SetSpeed(int speed)
    {
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
    }
}
