﻿using Assets.Scripts;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.Match;

public class MatchListPanel : MonoBehaviour
{
    [SerializeField]
    public JoinButton joinButtonPrefab;
    public GameObject matchList;

    // Start is called before the first frame update
    private void Awake()
    {
        matchList = GameObject.Find("MatchListPlane");
        AvailableMatchesList.OnAvailableMatchesChanged += AvailableMatchesList_OnAvailableMatchesChanged;
    }

    private void AvailableMatchesList_OnAvailableMatchesChanged(List<MatchInfoSnapshot> matches)
    {
        if (matchList != null) { 
            ClearExistingButtons();
            CreateNewJoinGameButtons(matches);
        }
    }

    private void ClearExistingButtons()
    {
        var buttons = matchList.GetComponentsInChildren<JoinButton>();
        foreach (var button in buttons)
        {
            Destroy(button.gameObject);
        }
    }

    private void CreateNewJoinGameButtons(List<MatchInfoSnapshot> matches)
    {
        foreach (var match in matches)
        {
            var button = Instantiate(joinButtonPrefab);
            button.Initialize(match, transform);
        }
    }


}
