﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
public class Boundary{
    public float xMin, xMax, zMin, zMax;
}

//ship movement, firing control

public class PlayerController : NetworkBehaviour
{
    [SyncVar]
    public int health;


    //movement Vars
    public float speed;
    public float tilt;
    public Boundary boundary;

    private Joystick joystick;

    //firing Vars
    public float fireRate = 0.5F;
    private float nextFire = 0.5F;
    private float myTime = 0.0F;

    private GameObject newProjectile;
    public GameObject shot;
    public Transform shotSpawn;

    void Start()
    {
          joystick = GameObject.Find("Joystick").GetComponent<Joystick>();
    }

    // Updates
    void Update()
    {
        //If not client's playerobject do not allow control
        if (!isLocalPlayer)
        {
            return;
        }

        //Shooting
        myTime = myTime + Time.deltaTime;
        if (Input.GetButton("Fire1") && myTime > nextFire)
        {
            nextFire = myTime + fireRate;
            CmdFire();

            nextFire = nextFire - myTime;
            myTime = 0.0F;

            GetComponent<AudioSource>().Play();
        }

        if (health <= 0)
        {
            CmdDeath(transform.gameObject);
        }
    }

    internal float GetHealth()
    {
        return health;
    }

    void FixedUpdate()
    {
        //If not client's playerobject do not allow control
        if (!isLocalPlayer)
        {
            return;
        }
        //MOVEMENT
        //float moveHorizontal = Input.GetAxis("Horizontal");
        //float moveVertical = Input.GetAxis("Vertical");

        float moveHorizontal = joystick.Horizontal;
        float moveVertical = joystick.Vertical;



        //keyboard controls --may delete
        if (Input.GetKeyDown(KeyCode.A))
        {
            moveHorizontal = -1;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            moveHorizontal = 1;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            moveVertical = -1;
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            moveVertical = 1;
        }

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        GetComponent<Rigidbody>().velocity = movement * speed;

        GetComponent<Rigidbody>().position = new Vector3(
            Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)
        );
        GetComponent<Rigidbody>().rotation = Quaternion.Euler(0.0f, 0.0f, GetComponent<Rigidbody>().velocity.x * -tilt);
        //END MOVEMENT

    }

    [Command]
    void CmdFire()
    {
        newProjectile = Instantiate(shot, shotSpawn.position, shotSpawn.rotation) as GameObject;
        NetworkServer.Spawn(newProjectile);
    }

    // Damage & death
    public void damage()
    {
        health--;
        //Debug.Log(health);
    }

    [Command]
    void CmdDeath(GameObject player)
    {
        NetworkServer.Destroy(player);
    }

    



}


