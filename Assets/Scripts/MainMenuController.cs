﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public GameObject shipPrefab;

    GameObject camera;
    int screenPos = 1;

    void Start()
    {
        camera = GameObject.Find("Camera");
    }

    // Update is called once per frame
    void Update()
    {
        switch (screenPos)
        {
            case 1:
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Instantiate(shipPrefab, new Vector3(-12, 8, -25), Quaternion.Euler(new Vector3(0, 27, 0)));
                    screenPos = 2;
                }
                break;
        }

        //touch?
        if (Input.touchCount == 1)
        {
            Debug.Log("TouchCount = 1");

            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                fp = touch.position;
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved) // update the last position based on where they moved
            {
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended) //check if the finger is removed from the screen
            {
                lp = touch.position;

                if (Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance)
                {//It's a drag
                    if ((lp.x > fp.x))  //If the movement was to the right)
                    {   //Right swipe
                        Debug.Log("#############RightSwipe");
                        if(screenPos>0) screenPos--;
                        
                    }
                    else
                    {   //Left swipe
                        Debug.Log("#############LeftSwipe");
                        if(screenPos<2)screenPos++;
                    }
                }

            }
            Debug.Log("#############" + screenPos);
        }
        
        camera.GetComponent<MMenuCamera>().SetCameraPos(Mathf.Clamp(screenPos, 0, 2));
    }
    private Vector3 fp;   //First touch position
    private Vector3 lp;   //Last touch position
    private float dragDistance;  //minimum distance for a swipe to be registered
    
    public int GetScreenPos()
    {
        return screenPos;
    }

}
