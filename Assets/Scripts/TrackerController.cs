﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class TrackerController : NetworkBehaviour
{
    public float moveSpeed;
    public float rotationSpeed;
    private GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        target = GetClosestPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        
        //target = GetClosestPlayer();
        //Debug.Log(target);

        GetComponent<Rigidbody>().velocity = transform.forward * moveSpeed;

        if (target != null)
        {
            if (transform.position.z > target.transform.position.z - 1)
            {
                var targetRotation = ClampRotationAroundXAxis(Quaternion.LookRotation(target.transform.position - transform.position));
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
            }
        }
        
    }



    GameObject GetClosestPlayer()
    {
        GameObject bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;

        foreach(GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            Vector3 directionToTarget = player.transform.position - transform.position;
            float dSqrToTarget = directionToTarget.sqrMagnitude;

            if(dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = player;
            }
        }

        return bestTarget;
    }




    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        /*
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);
        angleX = Mathf.Clamp(angleX, -45, 45);
        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
        */
        q.eulerAngles = new Vector3(0, Mathf.Clamp(q.eulerAngles.y, 115, 245), 0);

        //q.SetEulerRotation(new Vector3(0, Mathf.Clamp(q.eulerAngles.y, 135, 225),0));

        return q;



    }

}
