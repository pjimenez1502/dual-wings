﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MMenuCamera : MonoBehaviour
{
    public int posNum;
    public Vector3[] positions;
    Vector3 currPos;

    private void Start()
    {
        currPos = positions[posNum];
    }

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, currPos, 1.5f * Time.deltaTime);
    }

    public void SetCameraPos(int nNewPos)
    {
        currPos = positions[nNewPos];
    }


}
