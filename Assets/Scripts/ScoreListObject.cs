﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreListObject : MonoBehaviour
{
    private Text scoreText;
    private int score;

    private void Awake()
    {
        scoreText = GetComponentInChildren<Text>();
    }

    public void Initialize(int score, Transform panelTransform, int index)
    {
        this.score = score;

        scoreText.text = score.ToString();
        transform.SetParent(panelTransform);
        transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        transform.localRotation = Quaternion.Euler(new Vector3(90, 0, 0));
        transform.localPosition = new Vector3(0,0,4-index);
    }
}
