﻿using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System.Collections.Generic;

public class FirebaseUpload : MonoBehaviour
{

    private DatabaseReference dbReference;

    private ScorePanel scorePanelScript;

    void Awake()
    {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://dual-wings.firebaseio.com/");
        dbReference = FirebaseDatabase.DefaultInstance.RootReference;

        scorePanelScript = GameObject.Find("ScorePanel").GetComponent<ScorePanel>();

        FirebaseGetData();
    }

    public void FirebaseUploadData(int score)
    {
        //GetComponent<NetworkGameController>().getScore();
        Debug.Log("Uploading score to Firebase");
        string key = dbReference.Push().Key;
        dbReference.Child("Scores").Child(key).SetValueAsync(score);

    }

    public void FirebaseGetData()
    {
        dbReference.Child("Scores").OrderByValue().LimitToLast(9).ValueChanged += (object sender2, ValueChangedEventArgs e2) =>
        {
            if (e2.DatabaseError != null)
            {
                Debug.LogError(e2.DatabaseError.Message);
                return;
            }

            if (e2.Snapshot != null && e2.Snapshot.ChildrenCount > 0)
            {
                List<int> scoreList = new List<int>();

                foreach (var childSnapshot in e2.Snapshot.Children)
                {
                    scoreList.Add(int.Parse(childSnapshot.Value.ToString()));
                }

                scoreList.Reverse();
                scorePanelScript.UpdatingList(scoreList);

            } else
            {
                Debug.Log("FIREBASE EMPTY");
            }
        };
    }
}

